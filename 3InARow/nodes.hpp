#ifndef NODES_HPP
#define NODES_HPP
#include <vector>

class Node;

class Nodes
{
public:
  static std::vector< Node * > instance();
  static std::vector< Node * > remove();
  static void add(Node *);
  static void swap(size_t, size_t);
};

#endif
