#ifndef NODE_HPP
#define NODE_HPP

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QCursor>
#include <vector>

enum class direction_t {
  VERTICAL,
  HORIZONTAL,
  NONE
};

enum class color_t {
  RED = Qt::GlobalColor::red,
  GREEN = Qt::green,
  BLUE = Qt::blue,
  YELLOW = Qt::yellow,
  MAGENTA = Qt::magenta
};

class Node : public QObject, public QGraphicsItem
{
public:
  Node(color_t color, size_t index, size_t diameter);
  Node(const Node & node);
  ~Node();

  void printPos() const;
  void moveToPos(QPointF point);

  void setHomePos(QPointF point);
  void setIndex(size_t index);

  QPointF getHomePos() const;
  size_t getIndex() const;
  Qt::GlobalColor getColor() const;

protected:
  void onAnimationFinished();

private:
  const size_t animationPeriodMS_ = 500;
  direction_t movingDir_;
  QPointF homePos_;
  color_t color_;
  size_t index_;
  size_t diameter_;

  QRectF boundingRect() const;
  QPainterPath shape() const;

  void paint(QPainter *painter, const QStyleOptionGraphicsItem * option, QWidget * widget);
  void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
  void mousePressEvent(QGraphicsSceneMouseEvent * event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
};

#endif
