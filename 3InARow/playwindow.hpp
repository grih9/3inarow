#ifndef PLAYWINDOW_HPP
#define PLAYWINDOW_HPP

#include <QGraphicsScene>
#include <QGraphicsView>
#include <vector>
#include <memory>
#include "node.hpp"

//extern std::vector< Node * > nodes_;
const size_t FIELD_SIZE = 9;
const size_t INTERVAL = 20;

class PlayWindow : public QGraphicsView
{
  Q_OBJECT

public:
  explicit PlayWindow(QWidget * parent = nullptr);
  ~PlayWindow();

private:
  size_t getElementIndex(QPointF & point) const;
  //std::vector<Node *> nodes_;
  QGraphicsScene * scene_;
};

#endif
