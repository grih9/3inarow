#include "nodes.hpp"
#include "node.hpp"
#include <iostream>
#include <memory>

static std::unique_ptr< std::vector< Node * > > nodes;

std::vector< Node * > Nodes::instance()
{
  if (!nodes) {
    std::cerr << "NODES EMPTY\n";
    nodes.reset(new std::vector< Node * >);
  }

  return *nodes;
}

void Nodes::add(Node * obj)
{
  if (!obj) {
    std::cerr << "OBJECT EMPTY\n";
    return;
  }

  nodes->push_back(obj);
}

void Nodes::swap(size_t index1, size_t index2)
{
  if (!nodes) {
    return;
  }
  auto it1 = (*nodes).begin();
  std::advance(it1, index1);
  auto it2 = (*nodes).begin();
  std::advance(it2, index2);
  std::cerr << "before\n";
  (*nodes)[index1]->printPos();
  (*nodes)[index2]->printPos();
  QPointF p = (*nodes)[index1]->getHomePos();
  (*nodes)[index1]->setHomePos((*nodes)[index2]->getHomePos());
  (*nodes)[index2]->setHomePos(p);
  (*nodes)[index2]->moveToPos((*nodes)[index2]->getHomePos());
  //Nodes::instance()[i]->setPos(Nodes::instance()[i]->homePos_);
  (*nodes)[index2]->setIndex(index1);
  (*nodes)[index1]->setIndex(index2);

  std::iter_swap(it1, it2);

  (*nodes)[index1]->setPos(Nodes::instance()[index1]->getHomePos());
  std::cerr << "after\n";
  (*nodes)[index1]->printPos();
  (*nodes)[index2]->printPos();
  //Nodes::instance()[0]->printPos();
  //Nodes::instance()[1]->printPos();


}
