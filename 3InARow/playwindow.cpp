#include "playwindow.hpp"
#include "ui_playwindow.h"
#include "windows.h"
#include <ctime>
#include <memory>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <iostream>

#include "nodes.hpp"
#include "node.hpp"

color_t randomiseColor()
{
  int i = rand() % 5;

  switch (i)
  {
  case 0:
  {
    return color_t::RED;
  }
  case 1:
  {
    return color_t::GREEN;
  }
  case 2:
  {
    return color_t::BLUE;
  }
  case 3:
  {
    return color_t::YELLOW;
  }
  case 4:
  {
    return color_t::MAGENTA;

  }
  }

}


PlayWindow::PlayWindow(QWidget * parent) :
  QGraphicsView(parent)
{
  scene_ = new QGraphicsScene(this);   // Инициализируем графическую сцену
  scene_->setItemIndexMethod(QGraphicsScene::NoIndex); // настраиваем индексацию элементов
  //QGraphicsScene * scene = &(*scene_); // ИЗВРАЩЕНИЕ
  setScene(scene_);  // Устанавливаем графическую сцену в graphicsView
  setRenderHint(QPainter::Antialiasing);    // Настраиваем рендер
  setCacheMode(QGraphicsView::CacheBackground); // Кэш фона
  //setViewportUpdateMode(BoundingRectViewportUpdate);
  setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

  //setMouseTracking(true);
  scene_->setSceneRect(0, 0, 600, 800);
  Nodes::instance();

  srand(time(0));

  for (size_t i = 0; i < FIELD_SIZE * FIELD_SIZE; i++)
  {
    size_t diameter = 40;
    Node * item = new Node(randomiseColor(), i, diameter);
    item->setPos(40 + diameter / 2 + (i / FIELD_SIZE) * (diameter + 20), 160 + diameter / 2 + (i % FIELD_SIZE) * (diameter + 20));
    item->setHomePos(item->pos());
    //item->printPos();

    Nodes::add(item);
    //Nodes::instance()[i]->printPos();
    scene_->addItem(item);
  }
}

PlayWindow::~PlayWindow()
{
  delete scene_; // ?
}

size_t PlayWindow::getElementIndex(QPointF &point) const
{
  if (point.x() > 370 && point.x() < 430 && point.y() > 170 && point.y() < 230)
  {
    return 0;
  }
  else if (point.x() > 370 && point.x() < 430 && point.y() > 270 && point.y() < 330)
  {
    return 1;
  }
  else
  {
    return 10;
  }
}

