#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent):
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  playWindow_ = new PlayWindow();
}

MainWindow::~MainWindow()
{
  delete ui;
}


void MainWindow::on_quitButton_clicked()
{
    this->close();
}

void MainWindow::on_PlayButton_clicked()
{
    playWindow_->show();
    this->hide();
}
