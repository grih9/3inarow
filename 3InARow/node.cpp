#include "node.hpp"

#include <iostream>
#include <cmath>
#include <iterator>
#include <algorithm>
#include <QGraphicsItemAnimation>
#include <QTimeLine>

#include "nodes.hpp"
#include "playwindow.hpp"

//std::vector<Node *> nodes_;

Node::Node(color_t color, size_t index, size_t diameter):
  QObject(),
  QGraphicsItem(),
  movingDir_(direction_t::NONE),
  homePos_(0, 0),
  color_(color),
  index_(index),
  diameter_(diameter)
{
}

Node::Node(const Node &node):
  QObject(),
  QGraphicsItem(),
  movingDir_(direction_t::NONE),
  color_(node.color_),
  index_(node.index_)
{
}

Node::~Node()
{
}
/*
void Node::swap(size_t i)
{
  if (Nodes::instance().empty())
  {
    throw std::invalid_argument("Empty nodes vector.");
  }
  auto it1 = Nodes::instance().begin();
  std::advance(it1, index_);
  auto it2 = Nodes::instance().begin();
  std::advance(it2, i);
  std::cerr << "before\n";
  Nodes::instance()[index_]->printPos();
  Nodes::instance()[i]->printPos();
  QPointF p = Nodes::instance()[index_]->getHomePos();
  Nodes::instance()[index_]->setHomePos(Nodes::instance()[i]->getHomePos());
  Nodes::instance()[i]->setHomePos(p);
  Nodes::instance()[i]->moveToPos(Nodes::instance()[i]->getHomePos());
  //Nodes::instance()[i]->setPos(Nodes::instance()[i]->homePos_);
  Nodes::instance()[i]->setIndex(index_);
  Nodes::instance()[index_]->setIndex(i);

  std::iter_swap(it1, it2);


  std::cerr << "after\n";
  Nodes::instance()[index_]->printPos();
  Nodes::instance()[i]->printPos();
  //Nodes::instance()[0]->printPos();
  //Nodes::instance()[1]->printPos();
}
*/
void Node::printPos() const
{
  std::cerr << "index: " << index_ << "\n";
  std::cerr << homePos_.x() << ", " << homePos_.y() << "\ncurr:";
  std::cerr << this->x() << ", " << this->y() << "\n";
}

void Node::setHomePos(QPointF point)
{
  homePos_.setX(point.x());
  homePos_.setY(point.y());
}

QPointF Node::getHomePos() const
{
  return homePos_;
}

void Node::moveToPos(QPointF point)
{
  QTimeLine * timer = new QTimeLine(animationPeriodMS_, this);
  QGraphicsItemAnimation * animation = new QGraphicsItemAnimation(timer);
  connect(timer, SIGNAL(finished()), SLOT(onAnimationFinished()));
  animation->setItem(this);
  animation->setTimeLine(timer);
  std::cout << point.x() << " " << point.y() << "\n";
  std::cout << homePos_.x() << " " << homePos_.y() << "\n";
  animation->setPosAt(1.0, point);
  timer->start();
}

void Node::setIndex(size_t index)
{
  index_ = index;
}

size_t Node::getIndex() const
{
  return index_;
}

Qt::GlobalColor Node::getColor() const //TODO: найти способ обойтись без вот этого вот
{
  switch (color_)
  {
  case color_t::RED:
  {
    return Qt::red;
  }
  case color_t::GREEN:
  {
    return Qt::green;
  }
  case color_t::BLUE:
  {
    return Qt::blue;
  }
  case color_t::YELLOW:
  {
    return Qt::yellow;
  }
  case color_t::MAGENTA:
  {
    return Qt::magenta;
  }
  }
}

void Node::onAnimationFinished()
{
  sender()->deleteLater();
}

QRectF Node::boundingRect() const
{
  return QRectF(-30, -30, diameter_, diameter_);
}

QPainterPath Node::shape() const
{
  QPainterPath path;
  path.addEllipse(-30, -30, diameter_, diameter_);
  return path;
}

void Node::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
  painter->setPen(Qt::black);
  painter->setBrush(getColor());
  painter->drawEllipse(-30, -30, diameter_, diameter_);
  Q_UNUSED(option);
  Q_UNUSED(widget);
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
{
  QPointF point = mapToScene(event->pos());

  double deltaX = point.x() - homePos_.x();
  double deltaY = point.y() - homePos_.y();

  if (movingDir_ == direction_t::NONE) {
    if ((std::fabs(deltaX) > diameter_ / 2) && (std::fabs(deltaX) > std::fabs(deltaY))) {
      movingDir_ = direction_t::HORIZONTAL;
    } else if ((std::fabs(deltaY) > diameter_ / 2) && (std::fabs(deltaX) < std::fabs(deltaY))) {
      movingDir_ = direction_t::VERTICAL;
    }
  }

  if (movingDir_ == direction_t::HORIZONTAL) {

    if ((index_ < FIELD_SIZE)) {

      if ((deltaX < diameter_ + INTERVAL) && (deltaX > 0)) {
        setPos(point.x(), y());
      }

    } else if ((index_ > (FIELD_SIZE * (FIELD_SIZE - 1) - 1))) {

      if ((-deltaX < diameter_ + INTERVAL) && (deltaX < 0)) {
        setPos(point.x(), y());
      }

    } else if (std::fabs(deltaX) < diameter_ + INTERVAL) {
      setPos(point.x(), y());
    }

  } else if (movingDir_ == direction_t::VERTICAL) {

    if (index_ % FIELD_SIZE == 0) {

      if ((deltaY < diameter_ + INTERVAL) && (deltaY > 0)) {
        setPos(x(), point.y());
      }

    } else if (index_ % FIELD_SIZE == 8) {

      if ((-deltaY < diameter_ + INTERVAL) && (deltaY < 0)) {
        setPos(x(), point.y());
      }

    } else if (std::fabs(deltaY) < diameter_ + INTERVAL) {
      setPos(x(), point.y());
    }

  }
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

  this->setCursor(QCursor(Qt::ClosedHandCursor));
  Q_UNUSED(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

  this->setCursor(QCursor(Qt::ArrowCursor));

  if (movingDir_ == direction_t::HORIZONTAL)
  {
    if (homePos_.x() - x() > diameter_ + 10)
    {
      Nodes::swap(index_, index_ - 9);
    }
    else if (x() - homePos_.x() > diameter_ + 10)
    {
      Nodes::swap(index_, index_ + 9);
    }
  }
  else if (movingDir_ == direction_t::VERTICAL)
  {
    if (homePos_.y() - y() > diameter_ + 10)
    {
      Nodes::swap(index_, index_ - 1);
    }
    else if (y() - homePos_.y() > diameter_ + 10)
    {
      Nodes::swap(index_, index_ + 1);
    }
  }

  this->setPos(homePos_);
  movingDir_ = direction_t::NONE;
  Q_UNUSED(event);

}

