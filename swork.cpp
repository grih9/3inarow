#include <array>

class Work {
public:
  Work();
  bool isDestroyable(size_t index, Object & oject);
  bool isColumnDestroyable(size_t index, object & object);
  
private:
  std::array< Object > points_;
}

enum class color_t {
  RED = 0xFF0000,
  GREEN = 0x00FF00,
  BLUE = 0x0000FF,
  YELLOW = 0xF7F21A,
  PURPLE = 0x9932CC
}

enum class direction_t {
  VERTICAL
  HORIZONTAL
}

class Object {
public:
  Object();
  color_t getColor() const;
  
private:
  static num = 0;
  QpointF xOy_;
  color_t color_;
  
}

using checkDestroy = std::function< bool(size_t, Object) >;

struct destroy_t {
  direction_t dir;
  checkDestroy * ex;
}

...->ex(index, object);

bool Work::isLineDestroyable(size_t index, Object & object, direction_t direction)
{
  if (index > 81) {
    return false; // для безопасности;
  }
  size_t r1, r2, place; // расстояние 1; расстояние 2; строка/столбец;
  if (dir == direction_t::VERTICAL) {
    r1 = 1;
	r2 = 2;
	place = index % 9;
  } else (dir == direction_t::HORIZONTAL) {
	r1 = 9;
	r2 = 18;
	place = index / 9;
  }
	switch (place):
	case 0:
   	{
	  if (points_[i + r1].getColor() != points_[i + r2].getColor()) {
		false;
      }
	  return object.getColor == points_[i + r1]
	}
	case 8:
	{
	  if (points_[i - r1].getColor() != points_[i - r2].getColor()) {
		false;
      }
	  
	  return object.getColor == points_[i - r1].getColor 
	}
	default:
	{
	  if (points_[i - r1].getColor() != points_[i + r1].getColor()) {
		return false;
	  }
	  if ((place != 1) && (points_[i - r1].getColor() != points_[i - r2].getColor())) {
	    return false;
	  }
	  if ((place != 7) && (points_[i + r1].getColor() != points_[i + r2].getColor())) {
	    return false;
	  }
	  
	  return object.getColor == points_[i - 1].getColor 
	}
  //}
}

// расстояния вместо двух функций. 1 и 2 для вертикал и 9 18 для горизонталь
bool Work::isColumnDestroyableDestroyable(size_t index, Object & object/*, direction_t direction*/)
{
  assert (index > 81);
  /*if (dir == direction_t::VERTICAL) {*/
	size_t place = index / 9;
	switch (place):
	case 0:
   	{
	  if (points_[i + 1].getColor() != points_[i + 2].getColor()) {
		false;
      }
	  return object.getColor == points_[i + 1]
	}
	case 8:
	{
	  if (points_[i - 1].getColor() != points_[i - 2].getColor()) {
		false;
      }
	  
	  return object.getColor == points_[i - 1].getColor 
	}
	default:
	{
	  if (points_[i - 1].getColor() != points_[i + 1].getColor()) {
		return false;
	  }
	  if ((place != 1) && (points_[i - 1].getColor() != points_[i - 2].getColor())) {
	    return false;
	  }
	  if ((place != 7) && (points_[i + 1].getColor() != points_[i + 2].getColor())) {
	    return false;
	  }
	  
	  return object.getColor == points_[i - 1].getColor 
	}
  //}
}
	
  //} else if (dir == direction_t::HORIZONTAL) {
	  
  }
  } else {
    throw std::runtime_error("Invalid Direction!");
  }
}